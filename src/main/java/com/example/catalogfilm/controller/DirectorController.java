package com.example.catalogfilm.controller;

import com.example.catalogfilm.model.Director;
import com.example.catalogfilm.service.DirectorService;
import com.example.catalogfilm.service.FilmService;
import lombok.RequiredArgsConstructor;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/director")
@RequiredArgsConstructor
public class DirectorController {
    private final DirectorService directorService;
    @PostMapping
    public ResponseEntity<?> addDirector(@RequestBody Director director) {
        var resultDirector = directorService.saveDirector(director);
        return ResponseEntity.ok(resultDirector);
    }

    @GetMapping
    public ResponseEntity<?> getDirector(@RequestParam("directorUuid")UUID directorUuid) {
        var director = directorService.getDirector(directorUuid);
        return ResponseEntity.ok(director);
    }
}
