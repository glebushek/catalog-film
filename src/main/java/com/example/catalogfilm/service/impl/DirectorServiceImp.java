package com.example.catalogfilm.service.impl;

import com.example.catalogfilm.model.Director;
import com.example.catalogfilm.repository.DirectorRepository;
import com.example.catalogfilm.service.DirectorService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DirectorServiceImp implements DirectorService {
    private final DirectorRepository directorRepository;
    @Override
    @SneakyThrows(ChangeSetPersister.NotFoundException.class)
    public Director getDirector(UUID directorUuid) {
        var directorOptional = directorRepository.findById(directorUuid);
        return directorOptional.orElseThrow(ChangeSetPersister.NotFoundException::new);
    }

    @Override
    public Director saveDirector(Director director) {
        return directorRepository.save(director);
    }
}
