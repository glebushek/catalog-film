package com.example.catalogfilm.service;

import com.example.catalogfilm.model.Director;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.UUID;

public interface DirectorService {
    Director getDirector(UUID directorUuid);
    Director saveDirector(Director director);
}
